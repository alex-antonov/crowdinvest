<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhoneAndParentIdAndIpsColumnsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('login')->after('id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->string('phone')->nullable();
            $table->ipAddress('reg_ip')->nullable();
            $table->ipAddress('active_ip')->nullable();
            $table->string('name')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('active_ip');
            $table->dropColumn('reg_ip');
            $table->dropColumn('phone');
            $table->dropColumn('parent_id');
            $table->dropColumn('login');
            $table->string('name')->nullable(false)->change();
        });
    }
}
