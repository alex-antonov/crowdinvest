<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepthAndRefDepthAndRefCountColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('depth')->nullable();
            $table->unsignedInteger('ref_depth')->nullable();
            $table->unsignedInteger('ref_count')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('depth');
            $table->dropColumn('ref_depth');
            $table->dropColumn('ref_count');
        });
    }
}
