<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        factory(App\User::class, 50)->make()->each(function ($user) {
            $parent = App\User::inRandomOrder()->limit(1)->first();
            if ($parent === null) {
                $user->depth = null;
                $user->ref_depth = null;
                $user->save();
                return;
            }
            $user->depth = $parent->depth + 1;
            $user->ref_depth = $parent->depth + 1;
            $user->save();
            $refCount = App\User::where('parent_id', $parent->id)->count();
            if ($refCount === 1) {
                ++$parent->ref_depth;
            }
            while ($parent !== null) {
                if (($user->depth - $parent->depth) > $parent->ref_depth) {
                    ++$parent->ref_depth;
                }
                ++$parent->ref_count;
                $parent->save();
                $parent = App\User::where('id', $parent->parent_id)->first();
            }
        });
    }
}
