<?php
/**
 * Created by PhpStorm.
 * User: a
 * Date: 3/12/19
 * Time: 7:37 PM
 */

namespace App\Services;


use App\Exceptions\GeolocationServiceException;
use GuzzleHttp\Client;

class GeolocationService
{
    /**
     * @var Client $client
     */
    private $client;

    /**
     * GeolocationService constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Getting latitude or longitude by ip using service https://www.geoplugin.com
     *
     * @param $ip
     * @throws GeolocationServiceException
     * @return array
     */
    public function getCoordinateByIP($ip)
    {
        try {
            $response = $this->client->get(env('GEOPLUGIN_URL'), [
                'query' => ['ip' => $ip]
            ]);
            $geodata = json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $e) {
            throw new GeolocationServiceException($e->getMessage());
        }
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new GeolocationServiceException(json_last_error_msg());
        }
        if (!array_key_exists('geoplugin_latitude', $geodata) || !array_key_exists('geoplugin_longitude', $geodata)) {
            throw new GeolocationServiceException('Response does not has coordinate');
        }
        $lat = floatval($geodata['geoplugin_latitude']);
        $lng = floatval($geodata['geoplugin_longitude']);
        if (!$this->validateLatitude($lat)) {
            throw new GeolocationServiceException('Latitude is not correct');
        }
        if (!$this->validateLongitude($lng)) {
            throw new GeolocationServiceException('Longitude is not correct');
        }
        return compact('lat', 'lng');
    }

    /**
     *  Blurring latitude or longitude about ~1-2 km
     *
     * @param float|string $value latitude or longitude
     * @return float
     */
    public function blur($value)
    {
        $bias = rand(0, 1000) / 50000;
        return round(rand(0, 1) ? ($value + $bias) : ($value - $bias), 7);
    }

    /**
     * Check is coordinate null (latitude and longitude equal 0).
     *
     * @param float $lat
     * @param float $lng
     * @return bool
     */
    public function isNullCoordinate($lat, $lng)
    {
        return $lat > -0.1 && $lat < 0.1 && $lng > -0.1 && $lng < 0.1;
    }

    /**
     * Validate correct value of latitude
     *
     * @param float|null $lat
     * @return bool
     */
    private function validateLatitude($lat)
    {
        return is_null($lat) || ($lat >= -90 && $lat <= 90);
    }

    /**
     * Validate correct value of longitude
     *
     * @param float|null $lng
     * @return bool
     */
    private function validateLongitude($lng)
    {
        return is_null($lng) || ($lng >= -180 && $lng <= 180);
    }
}