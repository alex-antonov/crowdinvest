<?php

namespace App\Jobs;

use App\Events\SetGeolocation;
use App\Services\GeolocationService;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SetGeolocationByIp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var User $user
     */
    private $user;

    /**
     * Create a new job instance.
     * @param User $user
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @param GeolocationService $geolocationService
     */
    public function handle(GeolocationService $geolocationService)
    {
        $coordinate = $geolocationService->getCoordinateByIP($this->user->reg_ip);
        if ($geolocationService->isNullCoordinate($coordinate['lat'], $coordinate['lng'])) {
            $this->user->lat = $coordinate['lat'];
            $this->user->lng = $coordinate['lng'];
        } else {
            $this->user->lat = $geolocationService->blur($coordinate['lat']);
            $this->user->lng = $geolocationService->blur($coordinate['lng']);
        }
        event(new SetGeolocation($this->user));
    }
}
