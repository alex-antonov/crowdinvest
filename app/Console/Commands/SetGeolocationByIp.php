<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use App\Jobs\SetGeolocationByIp as Job;

class SetGeolocationByIp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geolocation:set-by-ip';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set geolocation by registration IP for all users without geolocation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @param Job $job
     * @return void
     */
    public function handle(Job $job)
    {
        $users = User::whereNull('lat')->whereNull('lng')->get();
        foreach ($users as $user) {
            $job::dispatch($user);
        }
    }
}
