<?php

namespace App\Listeners;

use App\User;
use Illuminate\Auth\Events\Registered;

class IncrementParentRefDepthAndRefCount
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $parentId = $event->user->parent_id;
        $parent = User::where('id', $parentId)->first();
        if ($this->isNewDepth($parentId)) {
            ++$parent->ref_depth;
        }
        while ($parent !== null) {
            if (($event->user->depth - $parent->depth) > $parent->ref_depth) {
                ++$parent->ref_depth;
            }
            ++$parent->ref_count;
            $parent->save();
            $parent = User::where('id', $parent->parent_id)->first();
        }
    }

    /**
     * Check if is a new depth level for parent
     *
     * @param int $userId
     * @return bool
     */
    private function isNewDepth($userId) : bool
    {
        $refCount = User::where('parent_id', $userId)->count();
        return $refCount === 1;
    }


}
