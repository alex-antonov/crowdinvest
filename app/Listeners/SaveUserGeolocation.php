<?php

namespace App\Listeners;

use App\Events\SetGeolocation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SaveUserGeolocation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SetGeolocation  $event
     * @return void
     */
    public function handle(SetGeolocation $event)
    {
        $event->user->save();
    }
}
